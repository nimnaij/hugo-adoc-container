# README #

This provides a simple image to strap hugo on top of asciidoctor.

### How does it change the standard image? ###

The repository will create an asciidoctor wrapper script that provides a more useful asciidoctor output to hugo. This includes disablig safe mode and loading an html5s.

### Usage ###

You can find the build for this docker at:

https://hub.docker.com/repository/docker/nimnaij/hugo-adoc

### I have a problem.  ###

For issues with this repo hit me up on twitter @nimnaij
