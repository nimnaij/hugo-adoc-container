FROM asciidoctor/docker-asciidoctor
ARG HUGO_VERSION=0.65.3

ENV HUGO_VERSION ${HUGO_VERSION}

RUN mkdir -p /opt/appbin 

RUN wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz -O - | tar -C /opt/appbin -xzvf - 

COPY bin/asciidoctor /opt/appbin/asciidoctor.tail

RUN printf '#!/bin/bash\n' > /opt/appbin/asciidoctor && \
    printf "REAL_ADOC='$(which asciidoctor)'\n" >> /opt/appbin/asciidoctor && \
    cat /opt/appbin/asciidoctor.tail >> /opt/appbin/asciidoctor && \ 
    rm /opt/appbin/asciidoctor.tail  && \ 
    chmod +x /opt/appbin/asciidoctor && \
    cat /opt/appbin/asciidoctor

RUN gem install asciidoctor-html5s
RUN gem install asciidoctor-mathematical

ENV PATH="/opt/appbin:${PATH}"

WORKDIR /opt/appbin
